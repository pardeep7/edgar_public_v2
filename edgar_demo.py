from edgar import ref_data, edgar_downloader, edgar_cleaner, edgar_sentiment_wordcount
import datetime as dt

# Download tickers
tickers = ref_data.get_sp100()

i = 0
tickers_used = []
for t in tickers:
    # Download raw html
    edgar_downloader.dow\nload_files_10k(t, './demo/10k_filings_raw')
    tickers_used.append(t)
    i+= 1
    if i == 2:\
        break

# Clean to txt
edgar_cleaner.write_clean_html_text_files('./demo/10k_filings_raw', './demo/10k_filings_clean')

# Sentiment word count
edgar_sentiment_wordcount.write_document_sentiments(r'./demo/10k_filings_clean', './demo/sentiment_factors.csv')

# Download yahoo financial data
yahoo_data = ref_data.get_yahoo_data('2009-01-01', str(dt.date.today()), tickers_used)
yahoo_data.to_csv('./demo/yahoo_data.csv')