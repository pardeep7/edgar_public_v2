# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 16:38:05 2021

@author: SunitaParmar
"""
import re
import os
from bs4 import BeautifulSoup

def clean_html_text(html_text):
    cleaned_html_text = re.sub(r'[^a-zA-Z0-9 ]','', html_text)
    return cleaned_html_text

def write_clean_html_text_files(input_folder, dest_folder):
    # Makes destination folder if doesn't exist already
    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)

    htmls = [file for file in os.listdir(input_folder)]
    # ^^ when I saved the htmls, a folder with images came with it so I just made a list of only the htmls in the folder
    for html in htmls:
        f = open(f'{input_folder}/{html}', 'rb')
        s = f.read()
        soup = BeautifulSoup(s.strip(),"html.parser")
        html_text = soup.text
        cleaned_html_text = clean_html_text(html_text)
        htmlsplit = html.split(".") #splits ticker_10-k_filing-date apart from .html ending
        with open(f'{dest_folder}/{htmlsplit[0]}.txt', "w+") as text_file:
            text_file.write(cleaned_html_text)