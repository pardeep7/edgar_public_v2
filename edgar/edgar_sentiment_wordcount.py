import pandas as pd
import numpy as np
import os
from edgar import ref_data
import time

def write_document_sentiments(input_folder: str, output_file: str):
    """

    Parameters
    ----------
    input_folder: str The input folder to be analysed
    output_file: str The output file to be saved to

    Returns
    -------
    None
    """

    stime = time.time()

    out = []

    word_sentiment_dict = ref_data.get_sentiment_word_dict()
    sentiment_categories = word_sentiment_dict.keys()

    # Loop through files
    for file in [file for file in os.listdir(input_folder)]:
        if os.path.getsize(f'{input_folder}/{file}') > 100000:
            print('reading file: %s' % file)
            # Read file
            f = open(f'{input_folder}/{file}', 'r')
            s = f.read()
            word_list = [word.lower() for word in s.split(" ")]

            # set up storage dict
            name = file.split("_")
            symbol = name[0]
            filing_date = name[2].split('.')[0]

            count_dict = {
                'Symbol' : symbol,
                'Report Type' : '10-k',
                'Filing date' : filing_date
            }
            for cat in sentiment_categories:
                count_dict[cat] = 0
                for word in word_list:
                    if word in word_sentiment_dict[cat]:
                        count_dict[cat] += 1

            out.append(count_dict)

    print(out)

    df = pd.DataFrame(out)

    df.to_csv(output_file)

    etime = time.time()

    print('Time taken: %.2fs' % (etime - stime))

    return None