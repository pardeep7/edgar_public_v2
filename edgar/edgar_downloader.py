# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:45:08 2021

@author: JoeSouth
"""

# Imports
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep
import os


def write_page(url: str, file_path: str):
    """

    Method to save the html for a url to a file

    Parameters
    ----------
    url : str
        the url to access and save
    file_path : str
        the file path to save the html text to

    Returns
    -------
    None.

    """
    try:
        r = requests.get(url)
    except:
        raise Exception('URL is either not reachable or does not exist')

    if r.status_code == 200:
        f = open(file_path, 'w+')
        f.write(r.text)
        f.close()
    else:
        raise Exception('Status code error: status code %i' % r.status_code)

    return None


def __get_10k_file_page(url: str, ticker: str):
    """


    Parameters
    ----------
    url : str
        The url of the edgar page
    ticker : str
        The ticker to search for, i.e. 'AAPL'

    Returns
    -------
    None.

    """

    driver = webdriver.Chrome()
    driver.get(url)

    print('Accessing page')

    search_input_path = r'/html/body/div[2]/div/div/div/section/div[3]/div[2]/div[2]/div[3]/div/form/input[1]'
    search_input = driver.find_element_by_xpath(search_input_path)
    search_input.send_keys(ticker, Keys.ENTER)

    print('Searching for ticker')

    sleep(1)

    filing_type_search_input_path = r'/html/body/div[4]/div[2]/form/table/tbody/tr[3]/td[1]/input'
    filing_type_search_input = driver.find_element_by_xpath(filing_type_search_input_path)
    filing_type_search_input.send_keys('10-k', Keys.ENTER)

    print('Searching for 10-k')

    sleep(1)

    include_button_path = r'//*[@id="include"]'
    include_button = driver.find_element_by_xpath(include_button_path)
    include_button.click()

    print('Clicking include')

    search_button_path = r'/html/body/div[4]/div[2]/form/table/tbody/tr[3]/td[5]/input[1]'
    search_button = driver.find_element_by_xpath(search_button_path)
    search_button.click()

    print('Clicking search')

    return driver


def __get_all_10k_urls(driver: webdriver.Chrome):
    '''
    Method to get the urls for all 10k documents

    Parameters
    ----------
    driver : selenium.webdriver.Chrome
        The driver with the loaded edgar page

    Returns
    -------
    links : list
        The list of links to the 10-k documents

    '''
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    table_soup = soup.find_all('table')
    base_url = 'http://sec.gov'
    links = []
    try:
        for row in table_soup[2].find_all('tr')[1:]:
            r = [col for col in row.find_all('td')]
            link_section = r[1]
            links.append([base_url + link_section.find_all('a')[0]['href'], r[3].text])
    finally:
        driver.close()
        return links


def __10k_url_to_file(url: str, dest_folder: str, ticker: str, filing_date: str):
    '''
    Method to save a 10k document as html given its url

    Parameters
    ----------
    url : str
        The url of the 10k document
        
    dest_folder : str
        the file path to save the html text to
        
    ticker : str
        The url of the 10k document

    Returns
    -------
    None.

    '''

    driver = webdriver.Chrome()
    driver.get(url)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    i_url = [x['href'] for x in soup.find_all('a')][9]
    base_url = r'http://sec.gov'
    driver.get(base_url + i_url)
    try:
        menu_path = r'//*[@id="menu-dropdown-link"]'
        menu_button = driver.find_element_by_xpath(menu_path)
        menu_button.click()
        open_as_html_path = r'//*[@id="form-information-html"]'
        open_as_html_button = driver.find_element_by_xpath(open_as_html_path)
        open_as_html_button.click()
        driver.switch_to.window(driver.window_handles[1])
    except:
        print('case 2')
    finally:
        raw_url = driver.current_url
        driver.close()

    name = '/%s_10k_%s.html' % (ticker, filing_date)

    write_page(raw_url, dest_folder + name)
    print('Link %s was successfully saved' % raw_url)


def download_files_10k(ticker: str, dest_folder: str):
    """

    Method to download all 10-k files for a given ticker to
    a given destination

    Parameters
    ----------
    ticker : str
        The ticker to get all files for

    dest_folder : str
        the path to save the html files to

    Returns
    -------
    None.

    """

    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)

    edgar_url = r'https://www.sec.gov/edgar/searchedgar/companysearch.html'

    try:
        driver = __get_10k_file_page(edgar_url, ticker)
    except:
        print('Issue finding ticker: %s' % ticker)
        return False

    link_date_pairs = __get_all_10k_urls(driver)

    for pair in link_date_pairs:
        if int(pair[1][0:4]) > 2008:
            try:
                print('Saving file: %s, %s' % (pair[0], pair[1]))
                __10k_url_to_file(pair[0], dest_folder, ticker, pair[1])
            except:
                print('There was an issue with this file: %s, %s' % (pair[0], pair[1]))
        else:
            print('File older than 2009, filing date %s' % pair[1])
