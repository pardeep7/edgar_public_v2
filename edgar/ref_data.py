"""
Created on Wed Feb 17 14:38:10 2021

@authors: PardeepJapper, Callum Ross, Danitza Sithirangathan
"""
#-----------------------------------------------------------------------------------
# Imports:
#-----------------------------------------------------------------------------------
import json
import requests
import pandas as pd
import datetime
import yfinance as yf
from bs4 import BeautifulSoup
from time import sleep
from typing import Dict 
#-----------------------------------------------------------------------------------
#Functions
#-----------------------------------------------------------------------------------
def get_sentiment_word_dict() -> Dict:
    '''
        Returns a dictionary of Loughran-McDonald (LM) Sentiment Words (lowercase)
        Source: https://sraf.nd.edu/textual-analysis/resources/#Master%20Dictionary 

        Key = sentiment
        Values = words assosciated with sentiment
    '''
    url_direct = r'http://drive.google.com/u/0/uc?id=15UPaF2xJLSVz8DYuphierz67trCxFLcl&export=download' # .xlsx file
    all_sheets_dict = pd.read_excel(io = url_direct, sheet_name = None, header = None, index_col = None)
    all_sheets_dict.pop('Documentation')

    sentiment_wordslist_dict = {}
    for sentiment, words in all_sheets_dict.items():
        words_list = [w.lower() for w in words.iloc[:, 0].to_list()]
        sentiment_wordslist_dict[sentiment] = words_list

    return sentiment_wordslist_dict



def get_sp100():
    '''
        Returns a snapshot list of the tickers of the S&P 100 Constituent Copmanies from the didvidendmax website
    '''

    tickers = []

    for i in range(1,5): # 30 companies per page 
        url = r'https://www.dividendmax.com/market-index-constituents/s-and-p-100?page=' + str(i)
        r = requests.get(url)
        soup = BeautifulSoup(r.text, 'html.parser')

        data = soup.find_all('td', {'class': 'mdc-data-table__cell'})

        data = [i.text.strip() for i in data][1::6] # table has 6 cols, tickers are in the second col

        tickers.extend(data)
    
    return sorted(tickers)



def get_yahoo_data(start_date, end_date, tickers):
    '''
        Returns a dataframe of historical price data for the supplied tickers and time period  
    '''
    df=pd.DataFrame()

    for i in range(len(tickers)):
        df1 = yf.download(tickers[i], start_date, pd.to_datetime(end_date) + datetime.timedelta(days=20))
        df1['1daily_return'] = -df1["Close"].diff(-1)
        df1['2daily_return'] = -df1["Close"].diff(-2)
        df1['3daily_return'] = -df1["Close"].diff(-3)
        df1['5daily_return'] = -df1["Close"].diff(-5)
        df1['10daily_return'] = -df1["Close"].diff(-10)
        df1['Symbol'] = tickers[i]
        df1 = df1[df1.index <= end_date]
        df = df.append(df1)


    df = df.rename(columns={"Close": "Price"})
    df = df.drop(columns=['Adj Close', 'Open'])

    return df