from edgar import ref_data, edgar_downloader

tickers = ref_data.get_sp100()
for t in tickers:
    edgar_downloader.download_files_10k(t, './10k_filings_raw')

