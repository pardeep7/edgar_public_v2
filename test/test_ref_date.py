from edgar import ref_data as rd
import pytest

def test_seven_keys_in_result():
        result = len(rd.get_sentiment_word_dict())
        expected_result = 7
        assert result == expected_result

def test_num_of_tickets():
        result = len(rd.get_sp100())
        expected_result = 97
        assert result == expected_result


class Store:
        test1 = ('2019-12-30', '2020-01-15', ['CAT'], 17)
        test2 = ('2020-01-05', '2020-02-05', ['AAPL'], 32)
        test3 = ('2019-12-30', '2020-01-15', ['CAT', 'AAPL'], 17*2)

        test_inputs = [test1, test2, test3]

@pytest.mark.parametrize("start_date, end_date, tickers, max_rows", Store.test_inputs)
def test_num_of_rows_one_ticker(start_date, end_date, tickers, max_rows):
        result = rd.get_yahoo_data(start_date, end_date, tickers).shape[0]
        assert result <= max_rows
