from edgar import edgar_cleaner as ch
import os

def test_clean_html_text():
    input_str = 'This sentence NeEds !$^leaning.'
    result = ch.clean_html_text(input_str)
    expected_result = 'This sentence NeEds leaning'
    assert result == expected_result


def test_write_clean_html_text_files():
    input_folder = r'.\test\test_input_folder'
    dest_folder = r'.\test\test_dest_folder'

    ch.write_clean_html_text_files(input_folder, dest_folder)

    result = len(os.listdir(dest_folder))
    expected_result = len(os.listdir(input_folder))
    assert result == expected_result